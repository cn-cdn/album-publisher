# album-publisher

Set 2 env variables in project settings:

```
export PRIVATE_KEY="-----BEGIN PRIVATE KEY-----
...
-----END PRIVATE KEY-----
"

export CLIENT_EMAIL='....iam.gserviceaccount.com'
```

Run as:

```
npm install -g gitlab:cn-cdn/album-publisher

export RELEASE_NAME="2013-the-little-mermaid"
export FOLDER_ID="1knk_FN_ByLkkRvNo-w0KghvTK9-zFXH0"

export CI_API_V4_URL="..."
export CI_PROJECT_ID="..."
export CI_JOB_TOKEN="..."

album-publisher
```
