#!/usr/bin/env node

const {mkdir, readdir, stat, copyFile, writeFile} = require('fs/promises');
const {createWriteStream, existsSync} = require('fs');
const stream = require('stream');
const {promisify} = require('util');
const process = require('node:process');
const path = require('node:path');
const {google} = require('googleapis');
const cliProgress = require('cli-progress');

const exec = promisify(require('child_process').exec);

const baseDir = 'files';
const publicDir = 'public';

if (!process.env.PRIVATE_KEY) {
	throw new Error('PRIVATE_KEY environment variable is not set');
}

if (!process.env.CLIENT_EMAIL) {
	throw new Error('CLIENT_EMAIL environment variable is not set');
}

if (!process.env.FOLDER_ID) {
	throw new Error('FOLDER_ID environment variable is not set');
}

if (!process.env.RELEASE_NAME) {
	throw new Error('RELEASE_NAME environment variable is not set');
}

const prepareDirectory = async filePath => {
	const dirName = path.dirname(filePath);
	await mkdir(dirName, {recursive: true});
};

/**
 * @param {string} cmd Command line string
 * @param {string} [cwd] Working directory
 */
const runCmd = async (cmd, cwd) => {
	try {
		cwd = cwd ?? '.';
		console.log(cmd);
		const options = {stdio: 'pipe', maxBuffer: 1024 * 500, cwd};
		await exec(cmd, options);
	} catch (error) {
		console.error(error.stdout);
		console.error(error.stderr);
		throw new Error(`${cmd} failed with code ${error.code}`);
	}
};

/**
 * @param {string} baseDir
 * @param {string} [dir]
 * @returns {Promise<string[]>}
 */
const walk = async (baseDir, dir) => {
	const curDir = dir ? path.join(baseDir, dir) : baseDir;

	let results = [];
	const list = await readdir(curDir);

	let i = 0;
	const next = async () => {
		const file = list[i++];
		if (!file) {
			return results;
		}

		const candidate = dir ? path.join(dir, file) : file;
		const fStat = await stat(path.join(baseDir, candidate));
		if (fStat?.isDirectory()) {
			results = results.concat(await walk(baseDir, candidate));
		} else {
			results.push(candidate);
		}

		await next();
	};

	await next();
	return results;
};

const applyConverter = async converter => {
	const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);

	const files = await walk(baseDir);

	bar.start(files.length, 0);
	for (const file of files) {
		await converter(file);
		bar.increment({filename: file});
	}

	bar.stop();
};

/**
 * @param {string} file
 * @param {string} outDir
 */
const simpleCopy = async (file, outDir) => {
	const input = path.join(baseDir, file);
	const output = path.join(outDir, file);
	await prepareDirectory(output);
	if (!existsSync(output)) {
		await copyFile(input, output);
	}
};

/**
 * @param {string} file
 */
const opusConverter = async file => {
	if (/\.(jpg|tif)$/.test(file)) {
		simpleCopy(file, path.join(publicDir, 'opus'));
		return;
	}

	if (!/\.flac$/.test(file)) {
		return;
	}

	const base = file.slice(0, file.lastIndexOf('.'));

	const input = path.join(baseDir, file);
	const output = path.join(publicDir, 'opus', base + '.opus');

	if (existsSync(output)) {
		return;
	}

	await prepareDirectory(output);

	console.log(`Creating ${output}`);
	// 160kbps
	// with https://wiki.hydrogenaud.io/index.php?title=Opus
	const cmd = `ffmpeg -i ${input} -vn -c:a libopus -b:a 160K ${output}`;
	await runCmd(cmd);
};

/**
 * @param {string} file
 */
const mp3Converter = async file => {
	if (/\.(jpg|tif)$/.test(file)) {
		simpleCopy(file, path.join(publicDir, 'mp3'));
		return;
	}

	if (!/\.flac$/.test(file)) {
		return;
	}

	const base = file.slice(0, file.lastIndexOf('.'));

	const input = path.join(baseDir, file);
	const output = path.join(publicDir, 'mp3', base + '.mp3');

	if (existsSync(output)) {
		return;
	}

	await prepareDirectory(output);

	console.log(`Creating ${output}`);
	// ~245 kbps
	// with https://wiki.hydrogenaudio.org/index.php?title=LAME#Recommended_encoder_settings
	const cmd = `ffmpeg -i ${input} -vn -c:a libmp3lame -qscale:a 0 ${output}`;
	await runCmd(cmd);
};

const pipeline = promisify(stream.pipeline);

/**
 * @returns {Promise<string>}
 */
async function fetchFiles() {
	const privateKey = process.env.PRIVATE_KEY;
	const clientEmail = process.env.CLIENT_EMAIL;
	const folderId = process.env.FOLDER_ID;
	const releaseName = process.env.RELEASE_NAME;

	console.log(`Connecting to Google Drive folder "${folderId}"...`);

	const scopes = [
		'https://www.googleapis.com/auth/drive.readonly',
	];

	const jwtClient = new google.auth.JWT(clientEmail, null, privateKey, scopes);
	await jwtClient.authorize();

	const drive = google.drive({version: 'v3', auth: jwtClient});

	const filenameFilter = process.env.FILENAME_FILTER ? new RegExp(process.env.FILENAME_FILTER) : null;

	/**
	 * @param  {string} folderId
	 * @param  {string} prefix
	 * @returns {Promise<{id: string, path: string}[]}>}
	 */
	const collectFiles = async (folderId, prefix) => {
		const out = [];

		const response = await drive.files.list({
			folderId,
			q: `'${folderId}' in parents and trashed = false`,
		});

		for (const item of response.data.files) {
			if (item.mimeType === 'application/vnd.google-apps.folder') {
				const items = await collectFiles(item.id, prefix + item.name + '/');
				out.push(...items);
			} else {
				const ext = path.extname(item.name);
				if (!['.flac', '.jpg', '.tif'].includes(ext)) {
					continue;
				}

				if (filenameFilter && !filenameFilter.test(item.name)) {
					continue;
				}

				out.push({
					id: item.id,
					path: prefix + item.name,
				});
			}
		}

		return out;
	};

	console.log('Downloading files...');
	const items = await collectFiles(folderId, `${baseDir}/`);

	const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);

	bar.start(items.length, 0);

	const downloadFile = async item => {
		const outPath = `./${item.path}`;

		if (existsSync(outPath)) {
			bar.increment();
			return;
		}

		await prepareDirectory(item.path);

		const file = createWriteStream(outPath);
		file.on('finish', () => {
			bar.increment();
		});

		const respFile = await drive.files.get({fileId: item.id, alt: 'media'}, {responseType: 'stream'});
		await pipeline(respFile.data, file);
	};

	const downloadables = [];
	for (const item of items) {
		downloadables.push(downloadFile(item));
	}

	await Promise.all(downloadables);
	bar.stop();

	return releaseName;
}

async function compress(format, albumName) {
	const outFile = `${albumName}-${format}.zip`;
	if (!existsSync(outFile)) {
		console.log(`Building ${format} files`);
		await runCmd(`zip -r ../../${outFile} . -1`, path.join(publicDir, format));
	}
}

async function collect() {
	const indexFile = path.join(publicDir, 'index.html');
	if (!existsSync(indexFile)) {
		console.log('Building index file');
		const files = await walk(publicDir);
		let out = files.map(file => `<a href="${file}">${file}</a>`).join('<br />');

		if (process.env.CI_API_V4_URL) {
			const uploadUrl = `${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/packages/generic/release/1.0.0`;
			const files = ['mp3', 'opus'].map(format => `${process.env.RELEASE_NAME}-${format}.zip`);
			out = files.map(file => `<a href="${uploadUrl}/${file}">${file}</a>`).join('<br />') + '<br /><br />' + out;
		}

		await writeFile(indexFile, out);
	}
}

async function publish() {
	if (!process.env.CI_API_V4_URL) {
		return;
	}

	for (const format of ['mp3', 'opus']) {
		const file = `${process.env.RELEASE_NAME}-${format}.zip`;
		const uploadUrl = `${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/packages/generic/release/1.0.0/${file}`;
		const jobToken = process.env.CI_JOB_TOKEN;
		const cmd = `curl --header "JOB-TOKEN: ${jobToken}" --upload-file ${file} ${uploadUrl}`;
		await runCmd(cmd);
	}
}

async function runApp() {
	const albumName = await fetchFiles();

	console.log('Building mp3 files');
	await applyConverter(mp3Converter);

	console.log('Building opus files');
	await applyConverter(opusConverter);

	for (const format of ['mp3', 'opus']) {
		await compress(format, albumName);
	}

	await collect();

	await publish();
}

runApp().catch(error => {
	console.error(error);
	process.exit(1);
});
